package com.lifengdi.graphql.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.lifengdi.graphql.domain.Author;
import com.lifengdi.graphql.domain.Book;
import com.lifengdi.graphql.service.AuthorService;
import com.lifengdi.graphql.service.BookService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author lifengdi
 * @date 2021/12/8 09:59
 */
@Component
public class Mutation implements GraphQLMutationResolver {
    @Resource
    private BookService bookService;

    @Resource
    private AuthorService authorService;

    public boolean newAuthor(String firstName, String lastName) {
        Author author = new Author();
        author.setLastName(lastName);
        author.setFirstName(firstName);
        return authorService.save(author);
    }

    public boolean saveBook(Book input) {
        return bookService.save(input);
    }
}
