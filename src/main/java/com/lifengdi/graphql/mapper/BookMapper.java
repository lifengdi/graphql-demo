package com.lifengdi.graphql.mapper;

import com.lifengdi.graphql.domain.Book;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lifengdi
* @description 针对表【t_book】的数据库操作Mapper
* @createDate 2021-12-03 18:18:04
* @Entity com.lifengdi.graphql.domain.Book
*/
public interface BookMapper extends BaseMapper<Book> {

}




