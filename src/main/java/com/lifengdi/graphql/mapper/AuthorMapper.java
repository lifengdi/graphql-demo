package com.lifengdi.graphql.mapper;

import com.lifengdi.graphql.domain.Author;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lifengdi
* @description 针对表【t_author】的数据库操作Mapper
* @createDate 2021-12-03 18:17:38
* @Entity com.lifengdi.graphql.domain.Author
*/
public interface AuthorMapper extends BaseMapper<Author> {

}




