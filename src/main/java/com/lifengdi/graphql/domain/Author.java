package com.lifengdi.graphql.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName t_author
 */
@TableName(value ="t_author")
@Data
public class Author implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private BigInteger id;

    /**
     * 创建时间
     */
    @TableField(value = "created_time")
    private Date createdTime;

    /**
     * 更新时间
     */
    @TableField(value = "updated_time")
    private Date updatedTime;

    /**
     * firstName
     */
    @TableField(value = "first_name")
    private String firstName;

    /**
     * lastName
     */
    @TableField(value = "last_name")
    private String lastName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}