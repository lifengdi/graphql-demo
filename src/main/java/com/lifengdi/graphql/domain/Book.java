package com.lifengdi.graphql.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName t_book
 */
@TableName(value ="t_book")
@Data
public class Book implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 创建时间
     */
    @TableField(value = "created_time")
    private Date createdTime;

    /**
     * 更新时间
     */
    @TableField(value = "updated_time")
    private Date updatedTime;

    /**
     * 标题
     */
    @TableField(value = "title")
    private String title;

    /**
     * 
     */
    @TableField(value = "author_id")
    private BigInteger authorId;

    /**
     * 
     */
    @TableField(value = "isbn")
    private String isbn;

    /**
     * 
     */
    @TableField(value = "page_count")
    private Integer pageCount;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}