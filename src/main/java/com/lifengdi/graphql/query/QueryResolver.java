package com.lifengdi.graphql.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.lifengdi.graphql.domain.Author;
import com.lifengdi.graphql.domain.Book;
import com.lifengdi.graphql.service.AuthorService;
import com.lifengdi.graphql.service.BookService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author lifengdi
 * @date 2021/12/7 17:47
 */
@Component
public class QueryResolver implements GraphQLQueryResolver {
    @Resource
    private BookService bookService;

    @Resource
    private AuthorService authorService;

    public List<Author> findAllAuthors() {
        return authorService.list();
    }

    public Author findAuthorById(Long id) {
        return authorService.getById(id);
    }

//    public List<Book> findAllBooks() {
//        return bookService.list();
//    }
//
//    public Book findBookById(Long id) {
//        return bookService.getById(id);
//    }

}
