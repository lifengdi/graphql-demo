package com.lifengdi.graphql.resolver;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.coxautodev.graphql.tools.GraphQLResolver;
import com.lifengdi.graphql.domain.Author;
import com.lifengdi.graphql.domain.Book;
import com.lifengdi.graphql.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author lifengdi
 * @date 2021/12/7 16:11
 */
@Component
@AllArgsConstructor
public class AuthorResolver implements GraphQLResolver<Author> {

    @Resource
    private BookService bookService;

    public List<Book> getBooks(Author author) {
        LambdaQueryWrapper<Book> lambdaQueryWrapper = new LambdaQueryWrapper<Book>().eq(Book::getAuthorId, author.getId());
        return bookService.list(lambdaQueryWrapper);
    }
}
