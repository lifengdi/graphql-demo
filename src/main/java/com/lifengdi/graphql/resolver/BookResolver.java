package com.lifengdi.graphql.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.lifengdi.graphql.domain.Author;
import com.lifengdi.graphql.domain.Book;
import com.lifengdi.graphql.service.AuthorService;
import org.dataloader.DataLoader;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.concurrent.CompletableFuture;

/**
 * @author lifengdi
 * @date 2021/12/7 17:36
 */
@Component
public class BookResolver implements GraphQLResolver<Book> {

    @Resource
    private AuthorService authorService;

    @Resource
    private DataLoader<BigInteger, Author> authorDataLoader;

    public CompletableFuture<Author> getAuthorBatch(Book book) {
        return authorDataLoader.load(book.getAuthorId());
//        return authorService.getById(book.getAuthorId());
    }

    public Author getAuthor(Book book) {
        return authorService.getById(book.getAuthorId());
    }
}
