package com.lifengdi.graphql.service;

import com.lifengdi.graphql.domain.Author;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lifengdi
* @description 针对表【t_author】的数据库操作Service
* @createDate 2021-12-03 18:17:38
*/
public interface AuthorService extends IService<Author> {

}
