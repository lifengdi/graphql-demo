package com.lifengdi.graphql.service;

import com.lifengdi.graphql.domain.Book;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lifengdi
* @description 针对表【t_book】的数据库操作Service
* @createDate 2021-12-03 18:18:04
*/
public interface BookService extends IService<Book> {

}
