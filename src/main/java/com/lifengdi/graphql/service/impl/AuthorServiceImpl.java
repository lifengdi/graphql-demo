package com.lifengdi.graphql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lifengdi.graphql.domain.Author;
import com.lifengdi.graphql.service.AuthorService;
import com.lifengdi.graphql.mapper.AuthorMapper;
import org.springframework.stereotype.Service;

/**
* @author lifengdi
* @description 针对表【t_author】的数据库操作Service实现
* @createDate 2021-12-03 18:17:38
*/
@Service
public class AuthorServiceImpl extends ServiceImpl<AuthorMapper, Author>
    implements AuthorService{

}




