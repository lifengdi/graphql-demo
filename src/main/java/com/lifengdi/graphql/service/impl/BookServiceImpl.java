package com.lifengdi.graphql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lifengdi.graphql.domain.Book;
import com.lifengdi.graphql.service.BookService;
import com.lifengdi.graphql.mapper.BookMapper;
import org.springframework.stereotype.Service;

/**
* @author lifengdi
* @description 针对表【t_book】的数据库操作Service实现
* @createDate 2021-12-03 18:18:04
*/
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book>
    implements BookService{

}




