package com.lifengdi.graphql;

import com.lifengdi.graphql.domain.Author;
import com.lifengdi.graphql.domain.Book;
import com.lifengdi.graphql.service.AuthorService;
import com.lifengdi.graphql.service.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.math.BigInteger;

@SpringBootTest
class GraphqlDemoApplicationTests {

    @Resource
    private AuthorService authorService;

    @Resource
    private BookService bookService;

    @Test
    void contextLoads() {
    }

    @Test
    void insertAuthor() {
        Author author = new Author();
        author.setFirstName("zhang");
        author.setLastName("san");
        authorService.save(author);
    }

    @Test
    void insertBook() {
        Book book = new Book();
        book.setAuthorId(BigInteger.valueOf(1));
        book.setIsbn("isbn");
        book.setTitle("金瓶梅");
        book.setPageCount(999);
        bookService.save(book);
    }

}
